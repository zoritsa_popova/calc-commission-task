### Install dependencies
`composer install --dev`

### Run the application
`php bin/console calculate-commission input.csv`

### Run the tests
`vendor\bin\phpunit tests`