<?php

namespace tests\Repository;

use App\Entity\Operation;
use App\Repository\OperationRepository;
use PHPUnit\Framework\TestCase;
use Zend\ServiceManager\ServiceManager;

class OperationRepositoryTest extends TestCase
{
    /** @var OperationRepository */
    private $operationRepository;

    protected function setUp(): void
    {
        $serviceManager = new ServiceManager(require __DIR__ . '/../../app/config/services.php');
        $this->operationRepository = $serviceManager->get(OperationRepository::class);
    }

    public function testAdd(): void
    {
        $operation = $this->operationRepository->add(
            '2019-11-02',
            '2',
            'legal',
            'cash_id',
            '120',
            'USD'
        );

        $this->assertInstanceOf(Operation::class, $operation);
    }

    public function testGetUserOperationsForTheWeek(): void
    {
        $this->operationRepository->add(
            '2019-10-01',
            '2',
            'legal',
            'cash_id',
            '10',
            'USD'
        );

        $operation = $this->operationRepository->add(
            '2019-11-02',
            '2',
            'legal',
            'cash_id',
            '120',
            'USD'
        );

        $this->operationRepository->add(
            '2019-11-02',
            '2',
            'legal',
            'cash_id',
            '10',
            'USD'
        );

        $this->operationRepository->add(
            '2019-11-03',
            '2',
            'legal',
            'cash_id',
            '10',
            'USD'
        );

        $this->operationRepository->add(
            '2019-11-01',
            '3',
            'legal',
            'cash_id',
            '10',
            'USD'
        );

        $this->assertEquals(2, count($this->operationRepository->getUserOperationsForTheWeek($operation)));
    }

    protected function tearDown(): void
    {
        $this->operationRepository = null;
    }
}