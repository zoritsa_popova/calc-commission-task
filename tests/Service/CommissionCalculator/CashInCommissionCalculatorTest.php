<?php

namespace tests\Service\CommissionCalculator;

use App\Repository\OperationRepository;
use App\Service\CommissionCalculator\CashInCommissionCalculator;
use App\Service\CurrencyConverter;
use PHPUnit\Framework\TestCase;
use Zend\ServiceManager\ServiceManager;

class CashInCommissionCalculatorTest extends TestCase
{
    /** @var OperationRepository */
    private $operationRepository;
    /** @var CurrencyConverter */
    private $currencyConverter;
    /** @var CashInCommissionCalculator */
    private $commissionCalculator;

    protected function setUp(): void
    {
        $serviceManager = new ServiceManager(require __DIR__ . '/../../../app/config/services.php');
        $this->operationRepository = $serviceManager->get(OperationRepository::class);
        $this->currencyConverter = new CurrencyConverter(['EUR', 'USD'], ['EUR' => 1, 'USD' => 1.5]);
        $this->commissionCalculator = new CashInCommissionCalculator($this->operationRepository, $this->currencyConverter);
    }

    /**
     * @dataProvider operationsProvider
     * @param string $date
     * @param string $userId
     * @param string $userType
     * @param string $operationType
     * @param string $amount
     * @param string $currencyCode
     * @param float $expectedCommission
     * @throws \Exception
     */
    public function testCalculateCommission(
        string $date,
        string $userId,
        string $userType,
        string $operationType,
        string $amount,
        string $currencyCode,
        float $expectedCommission
    ): void
    {
        $operation = $this->operationRepository->add(
            $date,
            $userId,
            $userType,
            $operationType,
            $amount,
            $currencyCode
        );

        $commission = $this->commissionCalculator->calculateCommission($operation);
        $this->assertEquals($expectedCommission, $commission);
    }

    public function operationsProvider(): array
    {
        return [
            ['2019-10-31', '1', 'legal', 'cash_in', '100', 'EUR', 0.03],
            ['2019-10-31', '1', 'legal', 'cash_in', '100000', 'EUR', 5],
            ['2019-10-31', '1', 'legal', 'cash_in', '100000', 'USD', 7.5],
        ];
    }

    protected function tearDown(): void
    {
        $this->operationRepository = null;
        $this->currencyConverter = null;
        $this->commissionCalculator = null;
    }

}