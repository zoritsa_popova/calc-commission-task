<?php

namespace tests\Service\CommissionCalculator;

use App\Repository\OperationRepository;
use App\Service\CommissionCalculator\NaturalPersonCashOutCommissionCalculator;
use App\Service\CurrencyConverter;
use PHPUnit\Framework\TestCase;
use Zend\ServiceManager\ServiceManager;

class NaturalPersonCashOutCommissionCalculatorTest extends TestCase
{
    /** @var OperationRepository */
    private $operationRepository;
    /** @var CurrencyConverter */
    private $currencyConverter;
    /** @var NaturalPersonCashOutCommissionCalculator */
    private $commissionCalculator;

    protected function setUp(): void
    {
        $serviceManager = new ServiceManager(require __DIR__ . '/../../../app/config/services.php');
        $this->operationRepository = $serviceManager->get(OperationRepository::class);
        $this->currencyConverter = new CurrencyConverter(['EUR', 'USD'], ['EUR' => 1, 'USD' => 1.5]);
        $this->commissionCalculator = new NaturalPersonCashOutCommissionCalculator($this->operationRepository, $this->currencyConverter);
    }

    /**
     * @dataProvider operationsProvider
     * @param string $date
     * @param string $userId
     * @param string $userType
     * @param string $operationType
     * @param string $amount
     * @param string $currencyCode
     * @param float $expectedCommission
     * @throws \Exception
     */
    public function testCalculateCommission(
        string $date,
        string $userId,
        string $userType,
        string $operationType,
        string $amount,
        string $currencyCode,
        float $expectedCommission
    ): void
    {
        $operation = $this->operationRepository->add(
            $date,
            $userId,
            $userType,
            $operationType,
            $amount,
            $currencyCode
        );

        $commission = $this->commissionCalculator->calculateCommission($operation);
        $this->assertEquals($expectedCommission, $commission);
    }

    public function testCalculateCommissionMultipleOperations(): void
    {
        $operations = [
            ['2019-10-31', '1', 'natural', 'cash_out', '100', 'EUR', 0],
            ['2019-11-01', '1', 'natural', 'cash_out', '1350', 'USD', 0],
            ['2019-11-01', '2', 'natural', 'cash_out', '1350', 'USD', 0],
            ['2019-11-01', '1', 'natural', 'cash_out', '100', 'EUR', 0.3],
            ['2019-11-11', '1', 'natural', 'cash_out', '100', 'EUR', 0],
        ];

        foreach ($operations as $operationRow) {
            $operation = $this->operationRepository->add(
                $operationRow[0],
                $operationRow[1],
                $operationRow[2],
                $operationRow[3],
                $operationRow[4],
                $operationRow[5]
            );

            $commission = $this->commissionCalculator->calculateCommission($operation);
            $this->assertEquals($operationRow[6], $commission);
        }
    }

    public function operationsProvider(): array
    {
        return [
            ['2019-10-31', '1', 'natural', 'cash_out', '100', 'EUR', 0],
            ['2019-11-01', '1', 'natural', 'cash_out', '1500', 'USD', 0],
            ['2019-11-01', '2', 'natural', 'cash_out', '1350', 'USD', 0],
            ['2019-11-01', '1', 'natural', 'cash_out', '1100', 'EUR', 0.3],
            ['2019-11-11', '1', 'natural', 'cash_out', '100', 'EUR', 0],
        ];
    }

    protected function tearDown(): void
    {
        $this->operationRepository = null;
        $this->currencyConverter = null;
        $this->commissionCalculator = null;
    }

}