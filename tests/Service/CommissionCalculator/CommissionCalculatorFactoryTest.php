<?php

namespace tests\Service\CommissionCalculator;

use App\Repository\OperationRepository;
use App\Service\CommissionCalculator\CashInCommissionCalculator;
use App\Service\CommissionCalculator\CommissionCalculatorFactory;
use App\Service\CommissionCalculator\LegalPersonCashOutCommissionCalculator;
use App\Service\CommissionCalculator\NaturalPersonCashOutCommissionCalculator;
use App\Service\CurrencyConverter;
use PHPUnit\Framework\TestCase;
use Zend\ServiceManager\ServiceManager;

class CommissionCalculatorFactoryTest extends TestCase
{
    /** @var OperationRepository */
    private $operationRepository;
    /** @var CurrencyConverter */
    private $currencyConverter;

    protected function setUp(): void
    {
        $serviceManager = new ServiceManager(require __DIR__ . '/../../../app/config/services.php');
        $this->operationRepository = $serviceManager->get(OperationRepository::class);
        $this->currencyConverter = new CurrencyConverter([], []);
    }

    public function testCreateCommissionCalculator(): void
    {
        $commissionCalculator = CommissionCalculatorFactory::createCommissionCalculator(
            'cash_in',
            'legal',
            $this->operationRepository,
            $this->currencyConverter
        );

        $this->assertInstanceOf(CashInCommissionCalculator::class, $commissionCalculator);

        $commissionCalculator = CommissionCalculatorFactory::createCommissionCalculator(
            'cash_in',
            'natural',
            $this->operationRepository,
            $this->currencyConverter
        );

        $this->assertInstanceOf(CashInCommissionCalculator::class, $commissionCalculator);

        $commissionCalculator = CommissionCalculatorFactory::createCommissionCalculator(
            'cash_out',
            'legal',
            $this->operationRepository,
            $this->currencyConverter
        );

        $this->assertInstanceOf(LegalPersonCashOutCommissionCalculator::class, $commissionCalculator);

        $commissionCalculator = CommissionCalculatorFactory::createCommissionCalculator(
            'cash_out',
            'natural',
            $this->operationRepository,
            $this->currencyConverter
        );

        $this->assertInstanceOf(NaturalPersonCashOutCommissionCalculator::class, $commissionCalculator);

    }

    public function testCreateCommissionCalculatorInvalidOperation(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("There is no commission service defined");

        $commissionCalculator = CommissionCalculatorFactory::createCommissionCalculator(
            'invalid_operation',
            'natural',
            $this->operationRepository,
            $this->currencyConverter
        );
    }

    public function testCreateCommissionCalculatorInvalidUserType(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("There is no commission service defined");

        $commissionCalculator = CommissionCalculatorFactory::createCommissionCalculator(
            'cash_out',
            'invalid_user_type',
            $this->operationRepository,
            $this->currencyConverter
        );
    }

    protected function tearDown(): void
    {
        $this->operationRepository = null;
        $this->currencyConverter = null;
    }
}