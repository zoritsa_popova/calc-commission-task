<?php

namespace tests\Service\DataStorage;

use App\Entity\Operation;
use App\Service\DataStorage\InMemoryDataStorage;
use PHPUnit\Framework\TestCase;

class InMemoryDataStorageTest extends TestCase
{
    public function testGetLastCollectionId(): void
    {
        $dataStorage = new InMemoryDataStorage();

        $this->assertEquals(0, $dataStorage->getLastCollectionId('test_collection'));

        $item = new \stdClass();
        $item->id = $dataStorage->getLastCollectionId('test_collection');
        $item->someProperty = 'someValue';
        $dataStorage->add('test_collection', $item);

        $this->assertEquals(1, $dataStorage->getLastCollectionId('test_collection'));

        $item = new \stdClass();
        $item->id = $dataStorage->getLastCollectionId('test_collection');
        $item->someProperty = 'someValue2';
        $dataStorage->add('test_collection', $item);

        $this->assertEquals(2, $dataStorage->getLastCollectionId('test_collection'));
    }

    public function testAdd(): void
    {
        $dataStorage = new InMemoryDataStorage();
        $item = new \stdClass();
        $item->id = $dataStorage->getLastCollectionId('test_collection');
        $item->someProperty = 'someValue';
        $dataStorage->add('test_collection', $item);

        $this->assertEquals(1, count($dataStorage->beginQuery('test_collection')->get()));

        $item = new \stdClass();
        $item->id = $dataStorage->getLastCollectionId('test_collection');
        $item->someProperty = 'someValue';
        $dataStorage->add('test_collection', $item);

        $this->assertEquals(2, count($dataStorage->beginQuery('test_collection')->get()));
    }

    public function testGet(): void
    {
        $dataStorage = new InMemoryDataStorage();

        $operation = new Operation();
        $operation->setUserId(2);
        $operation->setUserType('legal');
        $operation->setCurrencyCode('EUR');
        $operation->setAmount(100);
        $operation->setDate(new \DateTime('2019-11-02'));
        $operation->setOperationType('cash_in');
        $operation->setId($dataStorage->getLastCollectionId('operations') + 1);
        $dataStorage->add('operations', $operation);

        $result = $dataStorage
            ->beginQuery('operations')
            ->addWhere('userId', 'equals',2)
            ->get();

        $this->assertEquals([$operation], $result);

        $result = $dataStorage
            ->beginQuery('operations')
            ->addWhere('operationType', 'equals','cash_out')
            ->get();

        $this->assertEquals([], $result);
    }

    public function testGetShouldThrowExceptionForInvalidPath(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("No collection selected");

        $dataStorage = new InMemoryDataStorage();
        $dataStorage
            ->addWhere('operationType', 'equals','cash_in')
            ->get();
    }
}