<?php

namespace tests\Service\FileReader;

use App\Service\FileReader\CsvFileReader;
use Exception;
use PHPUnit\Framework\TestCase;

class CsvFileReaderTest extends TestCase
{
    public function testReadFile(): void
    {
        $fileReader = new CsvFileReader();

        $filePath = 'tests/input.csv';
        $result = $fileReader->readFile($filePath, ',');

        $this->assertInstanceOf(\Generator::class, $result);
    }

    public function testReadFileShouldThrowExceptionForInvalidPath(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Invalid file path");

        $filePath = 'tests/wrong-input.csv';
        $fileReader = new CsvFileReader();

        $result = $fileReader->readFile($filePath, ',');
        $result->next();
    }

}