<?php

namespace App\Service;

use Exception;
use PHPUnit\Framework\TestCase;

/**
 * Class CurrencyConverter
 * @package App\Service
 */
class CurrencyConverterTest extends TestCase
{
    public function testConvert(): void
    {
        $converter = new CurrencyConverter(
            ['EUR', 'USD', 'BGN'],
            ['EUR' => 1, 'USD' => 1.5, 'BGN' => 1.9554]
        );

        $amount = $converter->convert(10, 'EUR', 'USD');
        $this->assertEquals(15, $amount);

        $amount = $converter->convert(10, 'EUR', 'EUR');
        $this->assertEquals(10, $amount);

        $amount = $converter->convert(15, 'USD', 'BGN');
        $this->assertEquals(19.554, $amount);
    }

    public function testConvertInvalidCurrency(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Currency rates are not defined");

        $converter = new CurrencyConverter(
            ['EUR', 'USD', 'BGN'],
            ['EUR' => 1, 'USD' => 1.5, 'BGN' => 1.9554]
        );

        $converter->convert(10, 'EUR', 'GBP');
    }

    public function testConvertInvalidCurrencyRate(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Invalid currency rate");

        $converter = new CurrencyConverter(
            ['EUR', 'USD', 'BGN'],
            ['EUR' => 1, 'USD' => 0, 'BGN' => 1.9554]
        );

        $converter->convert(10, 'EUR', 'USD');
    }
}