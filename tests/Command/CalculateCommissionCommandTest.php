<?php

namespace tests\Command;

use App\Command\CalculateCommissionCommand;
use App\Repository\OperationRepository;
use App\Service\FileReader\FileReaderInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Zend\ServiceManager\ServiceManager;

class CalculateCommissionCommandTest extends TestCase
{
    /** @var OperationRepository */
    private $operationRepository;
    /** @var FileReaderInterface */
    private $fileReader;
    /** @var CommandTester */
    private $commandTester;

    protected function setUp(): void
    {
        $application = new Application();
        $serviceManager = new ServiceManager(require __DIR__ . '/../../app/config/services.php');
        $this->fileReader = $serviceManager->get('file_reader');
        $this->operationRepository = $serviceManager->get(OperationRepository::class);
        $application->add(new CalculateCommissionCommand($this->fileReader, $this->operationRepository));
        $command = $application->find('calculate-commission');
        $this->commandTester = new CommandTester($command);
    }

    protected function tearDown(): void
    {
        $this->operationRepository = null;
        $this->fileReader = null;
        $this->commandTester = null;
    }

    public function testExecute(): void
    {
        $filePath = 'tests/input.csv';

        $this->commandTester->execute(['file_path' => $filePath]);

        $expected = "0.60\r\n";
        $expected .= "3.00\r\n";
        $expected .= "0.00\r\n";
        $expected .= "0.06\r\n";
        $expected .= "0.90\r\n";
        $expected .= "0.00\r\n";
        $expected .= "0.70\r\n";
        $expected .= "0.30\r\n";
        $expected .= "0.30\r\n";
        $expected .= "5.00\r\n";
        $expected .= "0.00\r\n";
        $expected .= "0.00\r\n";
        $expected .= "8611.41\r\n";

        $this->assertEquals($expected, $this->commandTester->getDisplay());
    }

    public function testExecuteShouldReturnMessageForInvalidFilePath(): void
    {
        $filePath = 'tests/wrong-input.csv';

        $this->commandTester->execute(['file_path' => $filePath]);
        $this->assertEquals("Invalid file path\r\n", $this->commandTester->getDisplay());
    }

    public function testExecuteShouldReturnMessageForInvalidData(): void
    {
        $filePath = 'tests/invalid-data-input.csv';

        $this->commandTester->execute(['file_path' => $filePath]);
        $this->assertEquals("Invalid data provided\r\n", $this->commandTester->getDisplay());
    }
}