<?php

namespace tests\Entity;

use App\Entity\Operation;
use PHPUnit\Framework\TestCase;

class OperationTest extends TestCase
{
    public function testOperationCreate(): void
    {
        $operation = new Operation();
        $operation->setUserId(2);
        $operation->setUserType('legal');
        $operation->setCurrencyCode('EUR');
        $operation->setAmount(100);
        $operation->setDate(new \DateTime('2019-11-02'));
        $operation->setOperationType('cash_in');
        $operation->setId(1);

        $this->assertEquals("legal", $operation->getUserType());
        $this->assertEquals("EUR", $operation->getCurrencyCode());
        $this->assertEquals("100", $operation->getAmount());
        $this->assertEquals(new \DateTime('2019-11-02'), $operation->getDate());
        $this->assertEquals("cash_in", $operation->getOperationType());
        $this->assertEquals(1, $operation->getId());
    }
}