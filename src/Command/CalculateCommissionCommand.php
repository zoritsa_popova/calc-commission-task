<?php namespace App\Command;

use App\Config;
use App\Repository\OperationRepository;
use App\Service\CommissionCalculator\CommissionCalculatorFactory;
use App\Service\CurrencyConverter;
use App\Service\FileReader\FileReaderInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


/**
 * Class CalculateCommissionCommand
 * @package App\Command
 */
class CalculateCommissionCommand extends Command
{
    /**
     * @var FileReaderInterface
     */
    private $fileReader;
    /**
     * @var OperationRepository
     */
    private $operationRepository;

    /**
     * CalculateCommissionCommand constructor.
     * @param FileReaderInterface $fileReader
     * @param OperationRepository $operationRepository
     */

    public function __construct(FileReaderInterface $fileReader, OperationRepository $operationRepository)
    {
        parent::__construct();

        $this->fileReader = $fileReader;
        $this->operationRepository = $operationRepository;
    }

    /**
     * Configures the current command.
     */
    public function configure() : void
    {
        $this->setName('calculate-commission')
            ->setDescription('Calculates commission by given file')
            ->addArgument('file_path', InputArgument::REQUIRED, 'Path to file, which contains operation records');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    public function execute(InputInterface $input, OutputInterface $output) : void
    {
        $config = Config::getInstance();
        $currencyConverter = new CurrencyConverter(
            $config->get('currencies'),
            $config->get('ratesToBaseCurrency')
        );

        $filePath = $input->getArgument('file_path');

        try {
            $commissions = [];
            foreach ($this->fileReader->readFile($filePath) as $line) {
                if(count($line) != 6) {
                    throw new \Exception("Invalid data provided");
                }

                $operation = $this->operationRepository->add($line[0], $line[1], $line[2], $line[3], $line[4], $line[5]);
                $commissionCalculator = CommissionCalculatorFactory::createCommissionCalculator(
                    $operation->getOperationType(),
                    $operation->getUserType(),
                    $this->operationRepository,
                    $currencyConverter
                );

                $commissions[] = $commissionCalculator->calculateCommission($operation);
            }

            foreach ($commissions as $commission) {
                $commission = ceil($commission * 100) / 100;
                $output->writeln(number_format($commission, 2, '.', ''));
            }
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
        }
    }
}