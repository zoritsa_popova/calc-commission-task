<?php

namespace App\Service\DataStorage;

/**
 * Interface DataStorageInterface
 * @package App\Service\DataStorage
 */
interface DataStorageInterface
{
    /**
     * @param string $collectionName
     * @return int
     */
    public function getLastCollectionId(string $collectionName) : int;

    /**
     * @param string $collectionName
     * @param $item
     */
    public function add(string $collectionName, $item) : void;

    /**
     * @param string $attribute
     * @param string $operator
     * @param $value
     * @return DataStorageInterface
     */
    public function addWhere(string $attribute, string $operator, $value) : self ;

    /**
     * @return array
     */
    public function get(): array;

    /**
     * @param string $collectionName
     * @return DataStorageInterface
     */
    public function beginQuery(string $collectionName): self;
}