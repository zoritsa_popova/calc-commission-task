<?php

namespace App\Service\DataStorage;

use Exception;

/**
 * Class InMemoryDataDataStorage
 * @package App\Service\DataStorage
 */
class InMemoryDataStorage implements DataStorageInterface
{
    /**
     * @var array
     */
    private $where = [];
    /**
     * @var array
     */
    private $items = [];
    /**
     * @var string
     */
    private $queryCollectionName;
    /**
     * @var array
     */
    private $lastId = [];

    /**
     * @return string
     */
    public function getQueryCollectionName(): string
    {
        if (!$this->queryCollectionName) {
            throw new Exception("No collection selected");
        }
        return $this->queryCollectionName;
    }

    /**
     * @param string $queryCollectionName
     */
    public function setQueryCollectionName(?string $queryCollectionName): void
    {
        $this->queryCollectionName = $queryCollectionName;
    }

    /**
     * @param string $collectionName
     * @return DataStorageInterface
     */
    public function beginQuery(string $collectionName): DataStorageInterface
    {
        $this->setWhere([]);
        $this->setQueryCollectionName($collectionName);
        return $this;
    }

    /**
     * @param string $collectionName
     * @return int
     */
    public function getLastCollectionId(string $collectionName): int
    {
        return (key_exists($collectionName, $this->lastId)) ? $this->lastId[$collectionName] : 0;
    }

    /**
     * @param string $collectionName
     * @param $item
     */
    public function add(string $collectionName, $item): void
    {
        if (!key_exists($collectionName, $this->items)) {
            $this->items[$collectionName] = [];
        }

        $this->items[$collectionName][] = $item;
        $this->incrementLastId($collectionName);
    }

    /**
     * @param string $collectionName
     */
    private function incrementLastId(string $collectionName): void
    {
        if (!key_exists($collectionName, $this->lastId)) {
            $this->lastId[$collectionName] = 0;
        }
        $this->lastId[$collectionName]++;
    }

    /**
     * @param Object $item
     * @return bool
     * @throws Exception
     */
    private function isSuitable($item): bool
    {
        if (empty($this->where)) {
            return true;
        }

        $isSuitable = true;

        foreach ($this->where as $condition) {
            $getterMethodName = 'get' . ucfirst($condition['attribute']);

            if (!method_exists($item, $getterMethodName)) {
                throw new \Exception("Invalid attribute name ");
            }

            switch ($condition['operator']) {
                case 'equals':
                    if ($item->$getterMethodName() !== $condition['value']) {
                        $isSuitable = false;
                    }
                    break;
                case 'not_equals':
                    if ($item->$getterMethodName() === $condition['value']) {
                        $isSuitable = false;
                    }
                    break;
                case 'greater_or_equals':
                    if ($item->$getterMethodName() < $condition['value']) {
                        $isSuitable = false;
                    }
                    break;
                case 'less_or_equals':
                    if ($item->$getterMethodName() > $condition['value']) {
                        $isSuitable = false;
                    }
                    break;
                default:
                    throw new Exception("Undefined operator");
            }
        }

        return $isSuitable;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function get(): array
    {
        $results = [];

        if (!key_exists($this->getQueryCollectionName(), $this->items)) {
            $this->items[$this->getQueryCollectionName()] = [];
        }

        if (!empty($this->items[$this->getQueryCollectionName()])) {
            foreach ($this->items[$this->getQueryCollectionName()] as $item) {
                if ($this->isSuitable($item)) {
                    $results[] = $item;
                }
            }
        }

        $this->finishQuery();

        return $results;
    }

    /**
     *
     */
    public function finishQuery()
    {
        $this->setWhere([]);
        $this->setQueryCollectionName(null);
    }

    /**
     * @param array $where
     */
    private function setWhere(array $where): void
    {
        $this->where = $where;
    }

    /**
     * @param string $attribute
     * @param string $operator
     * @param $value
     * @return DataStorageInterface
     */
    public function addWhere(string $attribute, string $operator, $value): DataStorageInterface
    {
        $this->where[] = [
            'attribute' => $attribute,
            'operator' => $operator,
            'value' => $value
        ];

        return $this;
    }

}