<?php

namespace App\Service\FileReader;

use Generator;

/**
 * Interface FileReaderInterface
 * @package App\Service\FileReader
 */
interface FileReaderInterface
{
    /**
     * @param string $filePath
     * @param string $delimiter
     * @return Generator
     */
    public function readFile(string $filePath, string $delimiter = ','): Generator;
}