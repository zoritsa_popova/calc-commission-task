<?php

namespace App\Service\FileReader;

use Exception;
use Generator;

/**
 * Class CsvFileReader
 * @package App\Service\FileReader
 */
class CsvFileReader implements FileReaderInterface
{
    /**
     * Max line length in the file
     */
    const MAX_LINE_LENGTH = 100;

    /**
     * @param string $filePath
     * @param string $delimiter
     * @return Generator
     * @throws Exception
     */
    public function readFile(string $filePath, string $delimiter = ','): Generator
    {
        if (!file_exists($filePath)) {
            throw new Exception("Invalid file path");
        }

        if (($handle = fopen($filePath, "r")) !== false) {
            while (($line = fgetcsv($handle, self::MAX_LINE_LENGTH, $delimiter)) !== FALSE) {
                yield $line;
            }

            fclose($handle);
        }
    }
}