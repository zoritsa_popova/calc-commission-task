<?php

namespace App\Service\CommissionCalculator;

use App\Repository\OperationRepository;
use App\Service\CurrencyConverter;
use Exception;

class CommissionCalculatorFactory
{
    /**
     * Commission calculator service factory
     * Creates service instance based on operation type and userType
     * @param string $operationType
     * @param string $userType
     * @param OperationRepository $operationRepository
     * @param CurrencyConverter $currencyConverter
     * @return CommissionCalculatorInterface
     * @throws Exception
     */
    public static function createCommissionCalculator(
        string $operationType,
        string $userType,
        OperationRepository $operationRepository,
        CurrencyConverter $currencyConverter
    ): CommissionCalculatorInterface
    {
        switch (true){
            case $operationType == 'cash_in':
                return new CashInCommissionCalculator($operationRepository, $currencyConverter);
            case $operationType == 'cash_out' && $userType == 'legal':
                return new LegalPersonCashOutCommissionCalculator($operationRepository, $currencyConverter);
            case $operationType == 'cash_out' && $userType == 'natural':
                return new NaturalPersonCashOutCommissionCalculator($operationRepository, $currencyConverter);
            default:
                throw new Exception('There is no commission service defined');
        }
    }
}