<?php

namespace App\Service\CommissionCalculator;

use App\Entity\Operation;
use App\Repository\OperationRepository;
use App\Service\CurrencyConverter;

/**
 * Interface CommissionCalculatorInterface
 * @package App\Service\CommissionCalculator
 */
interface CommissionCalculatorInterface
{
    /**
     * CommissionCalculatorInterface constructor.
     * @param OperationRepository $operationRepository
     * @param CurrencyConverter $currencyConverter
     */
    public function __construct(OperationRepository $operationRepository, CurrencyConverter $currencyConverter);

    /**
     * @param Operation $operation
     * @return float
     */
    public function calculateCommission(Operation $operation): float;

}