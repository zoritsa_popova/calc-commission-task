<?php

namespace App\Service\CommissionCalculator;

use App\Entity\Operation;
use App\Repository\OperationRepository;
use App\Service\CurrencyConverter;

/**
 * Class NaturalPersonCashOutCommissionCalculator
 * @package App\Service\CommissionCalculator
 */
class NaturalPersonCashOutCommissionCalculator implements CommissionCalculatorInterface
{
    /**
     *Predefined commission percent
     */
    const COMMISSION_PERCENT = 0.3;
    /**
     *Predefined maximum number of operations with discount per week
     */
    const MAXIMUM_OPERATIONS_WITH_DISCOUNT_PER_WEEK = 3;
    /**
     * Predefined maximum amount without commission fee per week
     */
    const MAXIMUM_AMOUNT_WITHOUT_COMMISSION_PER_WEEK = 1000;
    /**
     *Currency for MAXIMUM_AMOUNT_WITHOUT_COMMISSION_PER_WEEK amount
     */
    const MAXIMUM_AMOUNT_WITHOUT_COMMISSION_PER_WEEK_CURRENCY = 'EUR';

    /**
     * @var OperationRepository
     */
    private $operationRepository;
    /**
     * @var CurrencyConverter
     */
    private $currencyConverter;
    /**
     * @var int
     */
    private $currentOperationId;
    /**
     * @var string
     */
    private $currentCurrency;
    /**
     * @var float
     */
    private $currentAmount;
    /**
     * Array of Operations that are:
     * -with the same userId
     * -with the same operationType
     * -within the same week
     * as the current operation
     * @var array
     */
    private $userOperationsThisWeek;
    /**
     * MAXIMUM_AMOUNT_WITHOUT_COMMISSION_PER_WEEK converted in $currentCurrency
     * @var float
     */
    private $maxAmountFreeOfChargeInCurrentCurrency;

    /**
     * NaturalPersonCashOutCommissionCalculator constructor.
     * @param OperationRepository $operationRepository
     * @param CurrencyConverter $currencyConverter
     */
    public function __construct(OperationRepository $operationRepository, CurrencyConverter $currencyConverter)
    {
        $this->operationRepository = $operationRepository;
        $this->currencyConverter = $currencyConverter;
    }

    /**
     * @param Operation $operation
     * @return float
     * @throws \Exception
     */
    public function calculateCommission(Operation $operation): float
    {
        $this->setCurrentOperationData($operation);
        $amountFreeOfCharge = $this->getAmountFreeOfCharge();

        return ($this->currentAmount - $amountFreeOfCharge) * self::COMMISSION_PERCENT * 0.01;
    }

    /**
     * Returns an amount for which no commission should be calculated
     *
     * @return float
     * @throws \Exception
     */
    private function getAmountFreeOfCharge(): float
    {
        $pastOperationsAmountTotal = $this->calculatePastOperationsAmountTotal();

        if (!$this->isDiscountApplicable($pastOperationsAmountTotal)) {
            return 0;
        }

        $leftForDiscount = $this->maxAmountFreeOfChargeInCurrentCurrency - $pastOperationsAmountTotal;

        if ($this->currentAmount > $leftForDiscount) {
            return $leftForDiscount;
        }

        return $this->currentAmount;
    }

    /**
     * @param Operation $operation
     * @throws \Exception
     */
    private function setCurrentOperationData(Operation $operation)
    {
        $this->currentCurrency = $operation->getCurrencyCode();
        $this->currentAmount = $operation->getAmount();
        $this->currentOperationId = $operation->getId();
        $this->userOperationsThisWeek = $this->operationRepository->getUserOperationsForTheWeek($operation);

        $this->maxAmountFreeOfChargeInCurrentCurrency = $this->currencyConverter->convert(
            self::MAXIMUM_AMOUNT_WITHOUT_COMMISSION_PER_WEEK,
            self::MAXIMUM_AMOUNT_WITHOUT_COMMISSION_PER_WEEK_CURRENCY,
            $this->currentCurrency
        );
    }

    /**
     * Returns the total amount of all $userOperationsThisWeek,
     * converted in $currentCurrency
     *
     * @return float
     * @throws \Exception
     */
    private function calculatePastOperationsAmountTotal(): float
    {
        $pastOperationsAmountTotal = 0;

        if (!empty($this->userOperationsThisWeek)) {
            foreach ($this->userOperationsThisWeek as $pastOperation) {
                $pastOperationsAmountTotal += $this->currencyConverter->convert(
                    $pastOperation->getAmount(),
                    $pastOperation->getCurrencyCode(),
                    $this->currentCurrency
                );
            }
        }

        return $pastOperationsAmountTotal;
    }

    /**
     * Checks is the discount can be applied
     *
     * @param float $pastOperationsAmountTotal
     * @return bool
     */
    private function isDiscountApplicable(float $pastOperationsAmountTotal): bool
    {
        if (count($this->userOperationsThisWeek) > self::MAXIMUM_OPERATIONS_WITH_DISCOUNT_PER_WEEK) {
            return false;
        }

        if ($pastOperationsAmountTotal >= $this->maxAmountFreeOfChargeInCurrentCurrency) {
            return false;
        }

        return true;
    }
}