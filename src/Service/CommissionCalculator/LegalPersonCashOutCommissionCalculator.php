<?php

namespace App\Service\CommissionCalculator;

use App\Entity\Operation;
use App\Repository\OperationRepository;
use App\Service\CurrencyConverter;
use Exception;

class LegalPersonCashOutCommissionCalculator implements CommissionCalculatorInterface
{
    /**
     *Predefined commission percent
     */
    const COMMISSION_PERCENT = 0.3;
    /**
     *redefined mix commission fee
     */
    const MINIMUM_COMMISSION_FEE = 0.5;
    /**
     * Currency for MINIMUM_COMMISSION_FEE amount
     */
    const MINIMUM_COMMISSION_FEE_CURRENCY = 'EUR';
    /**
     * @var OperationRepository
     */
    private $operationRepository;
    /**
     * @var CurrencyConverter
     */
    private $currencyConverter;
    /**
     * @var string
     */
    private $currentCurrency;
    /**
     * @var float
     */
    private $currentAmount;
    /**
     * MINIMUM_COMMISSION_FEE converted in $currentCurrency
     * @var float
     */
    private $minCommissionFeeInCurrentCurrency;

    /**
     * CashInCommissionCalculator constructor.
     * @param OperationRepository $operationRepository
     * @param CurrencyConverter $currencyConverter
     */
    public function __construct(OperationRepository $operationRepository, CurrencyConverter $currencyConverter)
    {
        $this->operationRepository = $operationRepository;
        $this->currencyConverter = $currencyConverter;
    }

    /**
     * @param Operation $operation
     * @return float
     * @throws Exception
     */
    public function calculateCommission(Operation $operation): float
    {
        $this->setCurrentOperationData($operation);
        $commission = $this->currentAmount * self::COMMISSION_PERCENT * 0.01;

        if ($commission < $this->minCommissionFeeInCurrentCurrency) {
            return $this->minCommissionFeeInCurrentCurrency;
        }

        return $commission;
    }

    /**
     * @param Operation $operation
     * @throws \Exception
     */
    private function setCurrentOperationData(Operation $operation)
    {
        $this->currentCurrency = $operation->getCurrencyCode();
        $this->currentAmount = $operation->getAmount();

        $this->minCommissionFeeInCurrentCurrency = $this->currencyConverter->convert(
            self::MINIMUM_COMMISSION_FEE,
            self::MINIMUM_COMMISSION_FEE_CURRENCY,
            $this->currentCurrency
        );
    }
}