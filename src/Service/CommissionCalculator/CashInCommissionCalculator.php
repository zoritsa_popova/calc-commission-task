<?php

namespace App\Service\CommissionCalculator;

use App\Entity\Operation;
use App\Repository\OperationRepository;
use App\Service\CurrencyConverter;

/**
 * Class CashInCommissionCalculator
 * @package App\Service\CommissionCalculator
 */
class CashInCommissionCalculator implements CommissionCalculatorInterface
{
    /**
     *Predefined commission percent
     */
    const COMMISSION_PERCENT = 0.03;
    /**
     *Predefined max commission fee
     */
    const MAXIMUM_COMMISSION_FEE = 5;
    /**
     *Currency for MAXIMUM_COMMISSION_FEE amount
     */
    const MAXIMUM_COMMISSION_FEE_CURRENCY = 'EUR';
    /**
     * @var OperationRepository
     */
    private $operationRepository;
    /**
     * @var CurrencyConverter
     */
    private $currencyConverter;
    /**
     * @var string
     */
    private $currentCurrency;
    /**
     * @var float
     */
    private $currentAmount;
    /**
     * MAXIMUM_COMMISSION_FEE converted in $currentCurrency
     * @var float
     */
    private $maxCommissionFeeInCurrentCurrency;

    /**
     * CashInCommissionCalculator constructor.
     * @param OperationRepository $operationRepository
     * @param CurrencyConverter $currencyConverter
     */
    public function __construct(OperationRepository $operationRepository, CurrencyConverter $currencyConverter)
    {
        $this->operationRepository = $operationRepository;
        $this->currencyConverter = $currencyConverter;
    }

    /**
     * @param Operation $operation
     * @return float
     * @throws \Exception
     */
    public function calculateCommission(Operation $operation): float
    {
        $this->setCurrentOperationData($operation);
        $commission = $this->currentAmount * self::COMMISSION_PERCENT * 0.01;

        if ($commission > $this->maxCommissionFeeInCurrentCurrency) {
            return $this->maxCommissionFeeInCurrentCurrency;
        }

        return $commission;
    }

    /**
     * @param Operation $operation
     * @throws \Exception
     */
    private function setCurrentOperationData(Operation $operation)
    {
        $this->currentCurrency = $operation->getCurrencyCode();
        $this->currentAmount = $operation->getAmount();

        $this->maxCommissionFeeInCurrentCurrency = $this->currencyConverter->convert(
            self::MAXIMUM_COMMISSION_FEE,
            self::MAXIMUM_COMMISSION_FEE_CURRENCY,
            $this->currentCurrency
        );
    }
}