<?php

namespace App\Service;

use Exception;

/**
 * Class CurrencyConverter
 * @package App\Service
 */
class CurrencyConverter
{
    /**
     * @var array
     */
    private $currencies = [];
    /**
     * @var array
     */
    private $currencyRates = [];

    /**
     * CurrencyConverter constructor.
     * @param array $currencies
     * @param array $currencyRates
     */
    public function __construct(array $currencies, array $currencyRates)
    {
        $this->currencies = $currencies;
        $this->currencyRates = $currencyRates;
    }

    /**
     * @param float $amount
     * @param string $fromCurrency
     * @param string $toCurrency
     * @return float
     * @throws Exception
     */
    public function convert(float $amount, string $fromCurrency, string $toCurrency): float
    {
        if (!key_exists($fromCurrency, $this->currencyRates) || !key_exists($toCurrency, $this->currencyRates)) {
            throw new Exception("Currency rates are not defined");
        }

        if ($this->currencyRates[$fromCurrency] <= 0 || $this->currencyRates[$toCurrency] <= 0) {
            throw new Exception("Invalid currency rate");
        }
        return $amount * $this->currencyRates[$toCurrency] / $this->currencyRates[$fromCurrency];
    }
}