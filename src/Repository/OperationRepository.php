<?php

namespace App\Repository;

use App\Entity\Operation;
use App\Service\DataStorage\DataStorageInterface;
use Exception;

/**
 * Class OperationRepository
 * @package App\Repository
 */
class OperationRepository
{
    /**
     * @var DataStorageInterface
     */
    private $dataStorage;
    private $operationsCollectionName = 'operations';

    public function __construct(DataStorageInterface $dataStorage)
    {
        $this->dataStorage = $dataStorage;
    }

    /**
     * @param string $userId
     * @param string $operationType
     * @param string $userType
     * @param string $date
     * @param string $amount
     * @param string $currencyCode
     * @return Operation
     * @throws Exception
     */
    public function add(
        string $date,
        string $userId,
        string $userType,
        string $operationType,
        string $amount,
        string $currencyCode
    ): Operation
    {
        $operation = new Operation();
        $operation->setId($this->dataStorage->getLastCollectionId($this->operationsCollectionName) + 1);
        $operation->setUserId((int)$userId);
        $operation->setOperationType($operationType);
        $operation->setDate(new \DateTime($date));
        $operation->setUserType($userType);
        $operation->setAmount((float)$amount);
        $operation->setCurrencyCode(strtoupper($currencyCode));

        $this->dataStorage->add($this->operationsCollectionName, $operation);
        return $operation;
    }

    /**
     * Returns array of Operations that are:
     * -with the same userId
     * -with the same operationType
     * -within the same week
     * as the $operation param
     *
     * @param Operation $operation
     * @return array
     * @throws Exception
     */
    public function getUserOperationsForTheWeek(Operation $operation): array
    {
        return $this->dataStorage->beginQuery($this->operationsCollectionName)
            ->addWhere('userId', 'equals', $operation->getUserId())
            ->addWhere('operationType', 'equals', $operation->getOperationType())
            ->addWhere('id', 'not_equals', $operation->getId())
            ->addWhere(
                'date',
                'greater_or_equals',
                clone $operation->getDate()->modify('Monday this week')
            )
            ->addWhere(
                'date',
                'less_or_equals',
                clone $operation->getDate()->modify('Sunday this week')
            )
            ->get();
    }
}