<?php

namespace App;

/**
 * Class Config
 * @package App
 */
final class Config
{
    /**
     * Path to configuration file
     */
    const CONFIG_FILE_PATH = '/app/config/config.php';
    /**
     * @var
     */
    private static $instance;
    /**
     * @var array
     */
    private $config = [];

    /**
     * Config constructor.
     */
    private function __construct()
    {
        $this->loadConfig();
    }

    /**
     * @return Config
     */
    public static function getInstance(): Config
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }


    /**
     * @throws \Exception
     */
    private function loadConfig(): void
    {
        if(!file_exists(__DIR__ . '/..' . self::CONFIG_FILE_PATH)) {
            throw new \Exception("Config file not found");
        }
        $this->config = include_once __DIR__ . '/..' . self::CONFIG_FILE_PATH;
    }

    /**
     * @param string $key
     * @return array|mixed
     */
    public function get(string $key)
    {
        $keys = explode('.', $key);
        $value = $this->config;

        foreach ($keys as $key) {
            $value = $value[$key];
        }

        return $value;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     *
     */
    private function __clone()
    {
    }

    /**
     *
     */
    private function __wakeup()
    {
    }
}