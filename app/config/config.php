<?php

return [
    'currencies' => [
        'EUR',
        'USD',
        'JPY'
    ],
    'ratesToBaseCurrency' => [
        'USD' => 1.1497,
        'JPY' => 129.53,
        'EUR' => 1
    ]
];
