<?php

use Interop\Container\ContainerInterface;

return [
    'services' => [
        'data_storage' => new App\Service\DataStorage\InMemoryDataStorage(),
        'file_reader' => new App\Service\FileReader\CsvFileReader(),
    ],
    'factories' => [
        App\Repository\OperationRepository::class => function(Zend\ServiceManager\ServiceManager $serviceManager) {
            return new App\Repository\OperationRepository(
                $serviceManager->get('data_storage')
            );
        },
        App\Command\CalculateCommissionCommand::class => function (Zend\ServiceManager\ServiceManager $serviceManager) {
            return new App\Command\CalculateCommissionCommand(
                $serviceManager->get('file_reader'),
                $serviceManager->get(App\Repository\OperationRepository::class)
            );
        },
    ],
];